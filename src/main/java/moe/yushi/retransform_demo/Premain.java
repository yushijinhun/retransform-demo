package moe.yushi.retransform_demo;

import static org.objectweb.asm.Opcodes.ASM8;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.instrument.Instrumentation;
import java.lang.instrument.UnmodifiableClassException;
import java.security.ProtectionDomain;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;

public class Premain {

	public static class MyClassFileTransformer implements ClassFileTransformer {
		private final String className;
		private final String originalStr;
		private final String replacement;

		public MyClassFileTransformer(String className, String originalStr, String replacement) {
			this.className = className;
			this.originalStr = originalStr;
			this.replacement = replacement;
		}

		@Override
		public byte[] transform(ClassLoader loader, String internalName, Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
			if (internalName != null && internalName.replace('/', '.').equals(className)) {
				System.err.println("Retransforming " + className);
				var reader = new ClassReader(classfileBuffer);
				var writer = new ClassWriter(ClassWriter.COMPUTE_MAXS);
				reader.accept(new ClassVisitor(ASM8, writer) {
					@Override
					public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
						return new MethodVisitor(ASM8, super.visitMethod(access, name, descriptor, signature, exceptions)) {
							@Override
							public void visitLdcInsn(Object value) {
								if (value instanceof String && value.equals(originalStr)) {
									super.visitLdcInsn(replacement);
									System.err.println("Found '" + value + "' in method " + name + descriptor);
								} else {
									super.visitLdcInsn(value);
								}
							}
						};
					}
				}, 0);
				return writer.toByteArray();
			} else {
				return null;
			}
		}
	}

	public static void premain(String arg, Instrumentation instrumentation) {
		if (arg == null || arg.isEmpty()) {
			System.err.println("Usage: foo.Bar::originalStr::replacement");
			System.exit(1);
		}

		var split = arg.split("::");
		var className = split[0];
		var originalStr = split[1];
		var replacement = split[2];

		instrumentation.addTransformer(new MyClassFileTransformer(className, originalStr, replacement), false);
	}

	public static void agentmain(String arg, Instrumentation instrumentation) {
		var split = arg.split("::");
		var className = split[0];
		var originalStr = split[1];
		var replacement = split[2];

		instrumentation.addTransformer(new MyClassFileTransformer(className, originalStr, replacement), true);

		for (var clazz : instrumentation.getAllLoadedClasses()) {
			if (className.equals(clazz.getName())) {
				System.err.println("Class found: " + clazz.getName());
				try {
					instrumentation.retransformClasses(clazz);
				} catch (UnmodifiableClassException e) {
					e.printStackTrace();
				}
				break;
			}
		}
	}
}
