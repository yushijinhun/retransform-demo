package moe.yushi.retransform_demo;

import java.io.File;
import com.sun.tools.attach.VirtualMachine;

public class Main {

	public static void main(String[] args) throws Exception {
		if (args.length == 0) {
			System.err.println("Usage: java -jar retransform_demo.jar <target_jvm_pid> foo.Bar::originalStr::replacement");
			System.exit(1);
		}

		var selfPath = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI())
				.getAbsolutePath();
		System.err.println("JAR path is " + selfPath);

		var jvm = VirtualMachine.attach(args[0]);
		System.err.println("Attached to JVM " + jvm.id());
		jvm.loadAgent(selfPath, args[1]);
		jvm.detach();
	}
}
