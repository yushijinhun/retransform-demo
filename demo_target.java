class DemoTarget {
	public static void main(String args[]) throws Exception {
		System.out.println("My PID is " + ProcessHandle.current().pid());
		for (;;) {
			Thread.sleep(1000L);
			doThings();
		}
	}
	private static void doThings() {
		System.out.println("ABC");
	}
}
