## at runtime
1. Compile using Gradle
	```
	$ gradle
	BUILD SUCCESSFUL in 4s
	3 actionable tasks: 3 executed
	```
2. Run demo_target
	```
	$ java demo_target.java
	My PID is 70132
	ABC
	ABC
	...
	```
3. In another terminal, attach the javaagent
	```
	$ java -jar build/libs/retransform-demo.jar 70056 DemoTarget::ABC::XYZ
	JAR path is /home/yushijinhun/repos/retransform-demo/build/libs/retransform-demo.jar
	Attached to JVM 70132
	```
4. Output of demo_target is changed
	```
	...
	ABC
	ABC
	ABC
	Class found: DemoTarget
	Retransforming DemoTarget
	Found 'ABC' in method doThings()V
	XYZ
	XYZ
	XYZ
	...
	```
## at startup
```
$ java -javaagent:build/libs/retransform-demo.jar=DemoTarget::ABC::XYZ demo_target.java
Retransforming DemoTarget
Found 'ABC' in method doThings()V
My PID is 73958
XYZ
XYZ
XYZ
```
